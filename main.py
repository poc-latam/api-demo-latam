import json
import os
from google.auth import jwt
from fastapi import FastAPI, HTTPException
from google.cloud import pubsub_v1
from google.cloud import bigquery

app = FastAPI()

service_account_info = json.load(open(os.getenv('GOOGLE_APPLICATION_CREDENTIALS')))
audience_pubsub = "https://pubsub.googleapis.com/google.pubsub.v1.Publisher"
credentials_pubsub = jwt.Credentials.from_service_account_info(service_account_info, audience=audience_pubsub)
publisher = pubsub_v1.PublisherClient(credentials=credentials_pubsub)

project_id = os.getenv('GOOGLE_CLOUD_PROJECT')
topic_name = os.getenv('MY_TOPIC_NAME')
full_topic_name = f"projects/{project_id}/topics/{topic_name}"


def publish_message(data):
    """Publicar un mensaje en el tópico de Pub/Sub."""
    message_data = json.dumps(data)
    future = publisher.publish(full_topic_name, message_data.encode("utf-8"))
    future.result()


@app.post("/send-message")
def send_message(message: dict):
    """Endpoint para enviar mensajes al tópico de Pub/Sub."""
    try:
        if "name" not in message or "type" not in message:
            raise HTTPException(status_code=400, detail="El mensaje debe contener las claves 'name' y 'type'.")
        publish_message(message)
        return {"status": "Mensaje enviado correctamente"}
    except Exception as e:
        return {"error": str(e)}


@app.post("/{topic_name}/{subscription_name}")
def get_last_message(topic_name: str, subscription_name: str):
    """Endpoint para obtener el último mensaje de una suscripción en Pub/Sub."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = f"projects/{project_id}/subscriptions/{subscription_name}"
    try:
        subscriber.get_subscription(subscription=subscription_path)
    except Exception as e:
        raise HTTPException(status_code=404,
                            detail=f"Subscription {subscription_name} does not exist for topic {topic_name}")

    response = subscriber.pull(
        request={"subscription": subscription_path, "max_messages": 1}
    )

    if response.received_messages:
        received_message = response.received_messages[0]
        message_data = received_message.message.data.decode("utf-8")
        subscriber.acknowledge(
            request={"subscription": subscription_path, "ack_ids": [received_message.ack_id]}
        )
        return {"last_message": message_data}
    else:
        return {"message": "No messages found in subscription."}


def run_bigquery_query(dataset_name, table_name):
    """Ejecutar una consulta en BigQuery."""
    client = bigquery.Client()
    query = f"""
        SELECT *
        FROM `{project_id}.{dataset_name}.{table_name}`
        LIMIT 20
    """
    query_job = client.query(query)
    rows = query_job.result()
    return rows


@app.post("/query-bigquery/{dataset_name}/{table_name}")
async def query_bigquery(dataset_name: str, table_name: str):
    """Endpoint para ejecutar una consulta en BigQuery."""
    try:
        result_rows = run_bigquery_query(dataset_name, table_name)
        result_data = [dict(row) for row in result_rows]
        return {"query_result": result_data}
    except Exception as e:
        return {"error": str(e)}