import os
import json
from fastapi.testclient import TestClient
from unittest import mock
from main import app

PROJECT_ID = os.getenv('GOOGLE_CLOUD_PROJECT')
TOPIC_NAME = os.getenv('TOPIC_NAME_TEST')
SUBSCRIPTION_NAME = os.getenv('SUBSCRIPTION_NAME_TEST')
DATASET_NAME = os.getenv('DATASET_NAME_TEST')
TABLE_NAME = os.getenv('TABLE_NAME_TEST')

test_message = {"name": "Pikachu", "type": "Electric"}

client = TestClient(app)


@mock.patch("main.pubsub_v1.PublisherClient.publish")
def test_send_message(mock_publish):
    response = client.post("/send-message", json=test_message)
    assert response.status_code == 200
    assert response.json() == {"status": "Mensaje enviado correctamente"}

    mock_publish.assert_called_once_with(f"projects/{PROJECT_ID}/topics/{TOPIC_NAME}",
                                         json.dumps(test_message).encode("utf-8"))
