# Use the jupyter/scipy-notebook image as the base.
FROM jupyter/scipy-notebook

# Define build arguments with default values.
ARG GOOGLE_CLOUD_PROJECT="CHANGE_THIS"
ARG GOOGLE_APPLICATION_CREDENTIALS="CHANGE_THIS"
ARG MY_TOPIC_NAME="CHANGE_THIS"
ARG TOPIC_NAME_TEST="CHANGE_THIS"
ARG SUBSCRIPTION_NAME_TEST="CHANGE_THIS"
ARG DATASET_NAME_TEST="CHANGE_THIS"
ARG TABLE_NAME_TEST="CHANGE_THIS"

# Set environment variables with default values.
ENV GOOGLE_CLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT
ENV GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS
ENV MY_TOPIC_NAME=$MY_TOPIC_NAME
ENV TOPIC_NAME_TEST=$TOPIC_NAME_TEST
ENV SUBSCRIPTION_NAME_TEST=$SUBSCRIPTION_NAME_TEST
ENV DATASET_NAME_TEST=$DATASET_NAME_TEST
ENV TABLE_NAME_TEST=$TABLE_NAME_TEST

# Copy your application code and requirements.txt to the container.
COPY requirements.txt .
COPY . .

# Copy the service account key to the container.
COPY $GOOGLE_APPLICATION_CREDENTIALS .

# Install dependencies.
RUN pip install --no-cache-dir -r requirements.txt

# Expose the port your FastAPI app will run on.
EXPOSE 8000

# Command to run your FastAPI app using uvicorn.
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
